package com.example.everaldo.gravarvideo;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {


    private Uri uriVideo;
    private VideoView videoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;
    public static MainActivity ActivityContext = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(mediaControls == null){
            mediaControls = new MediaController(this);
        }

        videoView = (VideoView) findViewById(R.id.videoView);

        Button botaoGravar = (Button) findViewById(R.id.gravarVideo);
        botaoGravar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                uriVideo = gerarUriArquivo(MEDIA_TYPE_VIDEO);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, uriVideo);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 10);
                startActivityForResult(intent, CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE);

            }
        });

        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

        if(!hasPermissions(this, PERMISSIONS)){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK){
            Toast.makeText(this, "Vídeo salvo em: " + data.getData(), Toast.LENGTH_LONG).show();
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Carregando Vídeo");
            progressDialog.setMessage("Carregando...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            try{
                videoView.setMediaController(mediaControls);
                videoView.setVideoURI(data.getData());
            }catch (Exception e){

            }
            videoView.requestFocus();
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    progressDialog.dismiss();
                    videoView.seekTo(position);
                    if(position == 0){
                        videoView.start();
                    }else{
                        videoView.pause();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Falha na captura do vídeo", Toast.LENGTH_LONG).show();
            Log.e("GravarVídeo", "Falha na captura do vídeo");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("position", videoView.getCurrentPosition());
        videoView.pause();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        position = savedInstanceState.getInt("position");
        videoView.seekTo(position);

    }

    private Uri gerarUriArquivo(int type){
        return Uri.fromFile(gerarArquivo(type));
    }

    private File gerarArquivo(int type){
        File diretorio = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), "Vídeos Gravados");
        if(! diretorio.exists()){
            if(! diretorio.mkdirs()){
                Toast.makeText(MainActivity.this, "Impossível criar diretório", Toast.LENGTH_LONG)
                        .show();
                Log.e("GravarVideo", "Impossível criar diretório");
                return null;
            }
        }

        Date data = new Date();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(data.getTime());

        File arquivo;

        if(type == MEDIA_TYPE_VIDEO){
            arquivo = new File(diretorio.getPath() + File.separator + "VIDEO_" + timeStamp + ".mp4");
        }
        else{
            return null;
        }
        return arquivo;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }



}
